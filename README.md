# AutomatedTikTokScraper

## What is this?

This is a tool which generates videos straight from tiktok, randomly at a set interval, with complete control of what pages it crawls, and what ratios of videos get in the output video.

It can detect songs used in videos via shazam and attempts to lookup its copyright holders using BMI's Repertoire database

## Cool, how do I use it?

Pretty simple here. routine.json does all of the work!

I'll explain the flow here 
```json
{
  "Weeks" : 0,
  "Days" : 0,
  "Hours" : 0,
  "Minutes" : 10,
  "Seconds" : 0,
  "Routine" : 0
}
```
This is the time interval, quite obvious, right? Missmash how often you would like your ```output.mp4``` to be... well... outputted.
Note that, I have not enabled any throttling, meaning, if you don't complete the work in time for the next schedule, it will not fire. You can change this very easily if you so wish.

```json
{
    "Fetch" : [
        ["https://tiktok.com/", 50]
        ],

        "Generate" : [
        ["topicmain", 20]
    ]
}
```

This fetched from well, your "main" tiktok, personalized for you. It will generate 50 of these videos. It is formatted as ```[page, amount of videos before returning].

topicmain is the word for all videos fetched from the tiktok home page.
There are other tags that follow this pattern. Such as topicfood, topicsports, topicdance. Any topic is in the format topicname 

```json
{
 "Restrictions" : {
      "RestrictHasSong" : true,
      "RestrictRights" : false,
      "RightsCriteria" : ["Elton John", "John Lennon", "John Coltrane", "Jon Anderson", "Sony", "SNY"]
    }
}
```
This is the control you have over music rights. 

"RestrictHasSong" will block if a video has a song as identified by shazam. This overrides RestrictRights

"RestrictRights" will block any mention of the following phrases anywhere in a video's text. If Sony or SNY is mentioned as a copyright holder, or in the description, it will be promptly removed from any possibility of being added to the ending compilation.

These are not perfect by any means. They are fallible; shazam can miss a song or it can return no copyright. It is not a perfect solution, a beat detector would be a nice addition to make RestrictHasSong more accurate.

## Woah cool! what are the requirements

I'm glad you asked ~~strawman~~ interested developer, you need

-- Chromedriver & Chromebrowser (for selenium)

-- Nim & Nimble (any recent version will do.)

-- Python <=3.0 & pip3

-- ```./nimbledeps.sh; pip install -r requirements.txt```

-- FFMPEG 

## How do I compile?

```nim c -d:release --threads:on main```

It does **not work** with -d:danger due to nimpy.

## How do I execute code every time an output.mp4 is written?

I've included a callback.sh. This is executed after your routine is done being stitched together :3

## Who developed this beautiful piece of software, made with love?

Caroline Marceano, solely.

With emotional support from Cassie, Alex, Jam Cat, Robib, Jacko.

And slight technical support from the Nim community.