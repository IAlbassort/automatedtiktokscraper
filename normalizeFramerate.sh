#!/bin/bash
x=$(ls processing/)
if [ $1 == "normalize" ]; then
    echo "Normalizing video please wait :3"
    for f in $x; do
        fps=$(ffprobe -v error -select_streams v -of default=noprint_wrappers=1:nokey=1 -show_entries stream=r_frame_rate processing/${f})
        profile=$(ffprobe -loglevel error -select_streams a -show_entries stream=profile -of default=nw=1 processing/${f})
        if test -z "$fps"; then
            rm processing/${f} 
            continue
        fi
        if [ $fps != "30/1" ]; then
            if [ $profile != "profile=LC" ] ; then
                ffmpeg -i processing/${f} -ac 1 -filter:v fps=30 -b:a 32k -y temp.mp4 &> /dev/null
            else
                ffmpeg -i processing/${f} -ac 1 -filter:v fps=30 -y temp.mp4 &> /dev/null
            fi

            rm processing/${f} 
            mv temp.mp4 processing/${f} 
        elif [ $profile != "profile=LC" ]; then
            ffmpeg -i processing/${f} -c:a aac -b:a 32k -y temp.mp4 &> /dev/null
            rm processing/${f} 
            mv temp.mp4 processing/${f} 
        fi
    done

elif [[ $1 == "removeDead" ]]; then
    for f in $x; do
        fps=$(ffprobe -v error -select_streams v -of default=noprint_wrappers=1:nokey=1 -show_entries stream=r_frame_rate processing/${f})
        if test -z "$fps"; then
            rm processing/${f} 
            continue
        fi
    done
else
    echo "Please use a valid input; this script is really only suppose to work with the nim included in the package."
    echo "There are only 2 valid inputs for the first paramater:"
    echo "1. removeDead: removes any media files which an fps cannot be gotten and thus are 'dead'"
    echo "2. normalize: converts all videos in normalize to 1/30 fps and a LC aac encoding, so they can be concatinated without any issues."
    exit 
fi